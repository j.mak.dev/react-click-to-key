import {Component} from 'react';
import {arrayOf, func, number} from 'prop-types';

const propTypes = {
  /**
   * The rendering function the key event handler will be passed to.
   */
  children: func.isRequired,
  /**
   * Event handler to be called on key event.
   */
  handler: func.isRequired,
  /**
   * Array of keys that the handler will be called on.
   */
  keys: arrayOf(number),
};

export default class ClickToKey extends Component {
  handleKeyFn = (e) => {
    const key = e.key || e.keyIdentifier || e.keyCode;

    if (this.props.keys.indexOf(key) > -1) {
      this.props.handler(e);
    }
  }

  render() {
    return this.props.children({keyHandler: this.handleKeyFn});
  }
}

ClickToKey.propTypes = propTypes;
ClickToKey.defaultProps = {
  keys: [13], // Enter key is the default.
};
